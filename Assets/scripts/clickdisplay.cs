﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class clickdisplay : MonoBehaviour {

	public static int clicks;

	Text text;
	
	void Awake () {
		clicks = 0;
        text = GetComponent<Text>();

	}

	void Update () {
		text.text = clicks.ToString();
	}

	void reset() {
		clicks = 0;
	}
}
