﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameController : MonoBehaviour {
	GameObject[,] field;
	public Color connected;
	public Color free;
	public GameObject onenode;
	public GameObject lnode;
	public GameObject inode;
	public GameObject tnode;
	public GameObject fournode;
    public Sprite level_1;
	bool changed;
	bool running;
    bool linesDrawn = false;
    GameObject pause;


	void Start () {
        loadLevelData(ApplicationModel.currentLevel);
	}

	void loadLevelData(int levelNr){
		string levelFuncName = "level" + levelNr.ToString();
		Debug.Log(levelFuncName);
        if (levelNr != 1)
            GameObject.Find("HelpText").gameObject.SetActive(false);
        pause = GameObject.Find("accessGranted");
        pause.SetActive(false);
		Invoke(levelFuncName, 0f);
		//easylevel();
		//level1();
		running = true;
		changed = false;
		Connector.free = free;
		Connector.conn = connected;
		checkcomplete();
	}

	void easylevel() {
		int[,] level = {{0,2,0}};
		makefield (1, 3, level);
	}

	void makefield(int x, int y, int[,] level){
		field = new GameObject[x, y];
		for(int i = 0; i < x; i++){
			for(int j = 0; j < y; j++){
				switch(level[i,j]){
				case(0): 
					field[i,j] = Instantiate(onenode, new Vector3((x-1)-(i*2f), 0.5f, (-y+1)+(j*2f)), Quaternion.identity) as GameObject;
					break;
				case(1):
					field[i,j] = Instantiate(lnode, new Vector3((x-1)-(i*2f), 0.5f, (-y+1)+(j*2f)), Quaternion.identity) as GameObject;
					break;
				case(2):
					field[i,j] = Instantiate(inode, new Vector3((x-1)-(i*2f), 0.5f, (-y+1)+(j*2f)), Quaternion.identity) as GameObject;
					break;
				case(3):
					field[i,j] = Instantiate(tnode, new Vector3((x-1)-(i*2f), 0.5f, (-y+1)+(j*2f)), Quaternion.identity) as GameObject;
					break;
				case(4):
					field[i,j] = Instantiate(fournode, new Vector3((x-1)-(i*2f), 0.5f, (-y+1)+(j*2f)), Quaternion.identity) as GameObject;
					break;
				}
				field[i,j].transform.parent = this.transform;
			}
		}
        Image image = GameObject.Find("Background").GetComponent<Image>();
        image.sprite = level_1;
	}

	void level5(){
		int[,] level = {{0, 3, 1, 0},
			            {1, 3, 4, 3},
			            {3, 2, 3, 2},
			            {1, 2, 1, 0}};
		makefield (4, 4, level);
	}

    void level1()
    {
        int[,] level = {{1, 1},
			            {0, 0}};
        makefield(2, 2, level);
    }

    void level2()
    {
        
        //image.sprite = Resources.Load<Sprite>("TEST_lightbeam_SpriteSheet_396x64_OneSpriteSize");
        //Debug.Log(image.sprite);
        int[,] level = {{1, 1},
			            {1, 1}};
        makefield(2, 2, level);
    }

    void level3()
    {
        int[,] level = {{0, 1, 0},
			            {3, 1, 0},
                        {1, 2, 1}};
        makefield(3, 3, level);
    }

    void level4()
    {
        int[,] level = {{1, 2, 0},
			            {3, 3, 1},
                        {0, 0, 0}};
        makefield(3, 3, level);
    }

    void level6()
    {
        int[,] level = {{0, 1, 1, 0},
			            {0, 4, 1, 1},
                        {1, 4, 1, 2},
                        {0, 0, 1, 1}};
        makefield(4, 4, level);
    }

    void level7()
    {
        int[,] level = {{0, 1, 0, 0},
			            {1, 4, 4, 1},
                        {2, 0, 3, 1},
                        {0, 0, 3, 1}};
        makefield(4, 4, level);
    }

    void level8()
    {
        int[,] level = {{1, 3, 2, 1},
			            {1, 4, 2, 1},
                        {1, 1, 0, 0},
                        {1, 3, 3, 1}};
        makefield(4, 4, level);
    }

    void level9()
    {
        
        int[,] level = {{1, 1, 0, 1, 0},
			            {2, 1, 3, 3, 3},
                        {1, 1, 0, 1, 1},
                        {1, 1, 1, 4, 1},
                        {1, 2, 1, 1, 1}};
        makefield(5, 5, level);
    }


    IEnumerator SwitchLevel()
    {
        pause.SetActive(true);
        yield return new WaitForSeconds(1);
        pause.SetActive(false);
        ApplicationModel.currentLevel++;
        Application.LoadLevel("minigame");

    }

	bool checkcomplete(){
        bool satisfied = true;
        Transform child;
        RaycastHit info;
        GameObject clone;
        //GameObject fence = new GameObject[x, y];
        
		this.BroadcastMessage("checkconnected");
        Debug.Log(transform.childCount);
		for(int i = 0; i < transform.childCount; i++){
			satisfied &= transform.GetChild(i).GetComponent<Connector>().connected;
			Debug.Log("checkcomplete: child# "+i+" connected: "+satisfied);
            if (!satisfied)
                break;
		}

        if (satisfied && transform.childCount != 0)
        {
            Debug.Log("LEVEL finished");
            //Delay for a while
            StartCoroutine(SwitchLevel());
            
        }

        
		return satisfied;
	}

    

    void Update()
    {
		if(running){
			if (changed){
				running = !checkcomplete();
				changed = false;
			}
			if(Input.GetMouseButtonUp(0)){
				RaycastHit info;
				Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
				if(Physics.Raycast(ray, out info, 20f, -1)){
					if(info.collider.tag == "box"){
						Debug.Log("clicked");
						clickdisplay.clicks++;
						info.collider.SendMessage("rotate");
						changed = true;
					}
				}
				running = !checkcomplete();
			}
		}
	}
}
