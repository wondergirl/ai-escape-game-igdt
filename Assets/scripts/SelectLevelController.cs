﻿using UnityEngine;
using System.Collections;



public class SelectLevelController : MonoBehaviour {

	public GameObject parentCanvas;

	// not less than 6
	public int nrOfLevels = 9;
	public Texture[] buttons = new Texture[9];

	bool buttonsSet = true;
	int nrPerRow = 3;


	void OnGUI(){
            GUI.backgroundColor = new Color(255, 255, 255, 0);

			int boxHeight = (int)(nrOfLevels/nrPerRow) * 170;
			int boxWidth = nrPerRow * 200;
			int nrRows = (int)(nrOfLevels/nrPerRow)+1;

	        GUI.BeginGroup (new Rect (Screen.width / 2 - 250, Screen.height / 2 - 250, boxWidth, boxHeight));
	        GUI.Box (new Rect (0,0,boxWidth, boxHeight), "");
	        int count = 1;

	        for (int j=1; j<=nrRows; j++){
		        for (int i=1; i <= nrPerRow && count <= nrOfLevels; i++){
			        
                    if (GUI.Button(new Rect(120 * (i - 1) + 40, 90 * j + 10, 200, 80), buttons[count-1]))
                    {
                    
			        	ApplicationModel.currentLevel = count;
         				Application.LoadLevel("minigame");
			        }
			        count = count + 1;
		        } 
		    }       
	        GUI.EndGroup (); 
	} 
}

