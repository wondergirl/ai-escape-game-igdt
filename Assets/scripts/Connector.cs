﻿using UnityEngine;
using System.Collections;

public class Connector : MonoBehaviour {
	public bool connected;
	public static Color conn;
	public static Color free;

	void start(){
		connected = false;
		this.renderer.material.color = free;
	}

	void rotate(){
		this.transform.Rotate(new Vector3(0f, 90f, 0f));
		this.audio.Play();
	}

	public bool checkconnected(){
		bool temp = true;
		Transform child;
		RaycastHit info;
		for(int i = 0; i < this.transform.childCount; i++){
			child = this.transform.GetChild(i);
			if(child.tag == "connector"){
				if(Physics.Raycast(child.position, Vector3.Normalize(child.position - this.transform.position), out info, 1f)){
					temp &= (info.collider.tag == "connector");
                    //Debug.DrawRay(child.GetChild(i).position, Vector3.Normalize(child.GetChild(i).position - child.position) * 0.3f, Color.cyan, 10f);
                    Debug.Log("connected " + i);

				} else {
					temp = false;
					break;
				}
			}
		}
        
		/*if(temp){
			this.renderer.material.color = conn;
		} else {
			this.renderer.material.color = free;
		} */
		connected = temp;
		return temp;
	}
}
