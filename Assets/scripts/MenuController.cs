﻿using UnityEngine;
using System.Collections;

public class MenuController : MonoBehaviour {

	public void startGame () {
		Application.LoadLevel("minigame");
	}

	public void quitGame(){
		Application.Quit();
	}

	public void selectLevel(){
		Application.LoadLevel("selectLevel");
	}

    public void backToMenu()
    {
        Application.LoadLevel("menu");
    }

    public void showCredits()
    {
        Application.LoadLevel("credits");
    }
}
