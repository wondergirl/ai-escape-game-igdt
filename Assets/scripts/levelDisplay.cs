﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class levelDisplay : MonoBehaviour
{

    public static int clicks;

    Text text;

    void Awake()
    {
        text = GetComponent<Text>();

    }

    void Update()
    {
        text.text = ApplicationModel.currentLevel.ToString();
    }
}
