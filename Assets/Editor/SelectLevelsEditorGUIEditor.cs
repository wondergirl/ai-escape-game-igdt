﻿using UnityEngine;
 
using UnityEditor;
[CustomEditor(typeof(SelectLevelController))]
public class SelectLevelsEditorGUIEditor : Editor
{
    private Texture[] temp = new Texture[9];

    int i;
 
    public override void OnInspectorGUI ()
    {
        serializedObject.Update();
		EditorGUILayout.PropertyField(serializedObject.FindProperty("parentCanvas"));

		EditorGUILayout.HelpBox("Introduce Size which is the number of levels.", MessageType.Info);
		EditorGUILayout.PropertyField(serializedObject.FindProperty("nrOfLevels"));

		EditorGUILayout.HelpBox("Introduce Size - number of images to be used which is the number of levels. Click enter. Drag the level textures to the respective levels", MessageType.Info);
		EditorGUILayout.PropertyField(serializedObject.FindProperty("buttons"), true);
		var myScript = target as SelectLevelController;


		serializedObject.ApplyModifiedProperties();
		EditorUtility.SetDirty(myScript);
    }
}
